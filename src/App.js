import React from 'react';
import { Provider } from 'mobx-react';
import classNames from 'classnames/bind';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {styles} from './App.css';
import QueryBuilder from './components/QueryBuilder';
import ListReps from './components/ListReps';
import RepInfo from './components/RepInfo';
import QueryBuilderStore from './stores/QueryBuilderStore';
import RepresentativeStore from './stores/RepresentativeStore';

const cx = classNames.bind(styles);

function App() {
  return (
    <Provider queryBuilderStore={QueryBuilderStore} representativeStore={RepresentativeStore}>
      <div className={cx('top')}>
        <ToastContainer />
        <h1 className={cx('header')}>
          Who's my Representative?
        </h1>

        <QueryBuilder/>
      </div>
      <div className={cx('container')}>
        <div className={cx('infoDisplay')}>
          <ListReps/>
          <RepInfo/>
        </div>
      </div>
    </Provider>
  );
}

export default App;
