import {observable, decorate} from 'mobx';


export class QueryBuilderStore {
	repType = '';
	usState = '';

	changeRepType (repType) {
		this.repType = repType;
	}
	changeUsState (usState) {
		this.usState = usState;
	}
}

decorate(QueryBuilderStore, {
	query: observable,
	repType: observable,
	usState: observable
});

export default new QueryBuilderStore();
