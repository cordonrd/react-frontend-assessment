import {observable, action, decorate} from 'mobx';
import { toast } from 'react-toastify';


export class RepresentativeStore {
	representatives = observable([]);
	selectedRep = '';
	repType = '';
	queryError = null;


	getRepresentatives (repType, usState) {
		if (!usState){
			toast.error('Please Select a US state.');
			return;
		}
		this.repType = repType;
		if (repType === 'representatives') {
			return fetch(`http://localhost:3001/representatives/${usState}`, {
				method: 'get',
				headers: new Headers({ 'Content-Type': 'application/json' })
			})
			.then(response => response.json())
			.then(body => {
				if(body.error) {
					toast.error(body.error);
					return;
				}
				this.representatives = body.results;
			});
		} else if (repType === 'senators') {
			return fetch(`http://localhost:3001/senators/${usState}`, {
				method: 'get',
				qs: {
					state: usState
				},
				headers: new Headers({ 'Content-Type': 'application/json' })
			})
			.then(response => response.json())
			.then(body => {
				if(body.error) {
					toast.error(body.error);
					return;
				}
				this.representatives = body.results;
			});
		} else {
			toast.error('Please select either Representatives or Senators');
		}
	};
	setSelectedRep(name) {
		this.selectedRep = name;
	}
}

decorate(RepresentativeStore, {
	representatives: observable,
	selectedRep: observable,
	queryError: observable,
	getRepresentatives: action,
	setSelectedRep: action
});

export default new RepresentativeStore();