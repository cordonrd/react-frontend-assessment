import React from 'react';
import {observer, inject} from 'mobx-react';
import { Table, TableBody, TableRow, TableCell, Paper, TableHead, TableContainer } from '@material-ui/core';
import { styles } from './listReps.css'
import classNames from 'classnames/bind';

const _ = require('lodash');
const cx = classNames.bind(styles);

class ListReps extends React.Component {
	render() {
		const representativeList = _.map(this.props.representativeStore.representatives, rep => {
			return _.pick(rep, ['name', 'party']);
		});
		return (
			<div className={cx('listRepsContainer')}>
				<h2>List / <span className={cx('repTypeSpan')}>{_.upperFirst(this.props.representativeStore.repType||'')}</span></h2>
				<TableContainer component={Paper}>
					<Table className={cx('tableBody')} aria-label="simple table">
						<TableHead>
						<TableRow>
							<TableCell>Name</TableCell>
							<TableCell align="right">Party</TableCell>
						</TableRow>
						</TableHead>
						<TableBody>
						{representativeList.map(row => (
							<TableRow 
								key={row.name} 
								hover
								onClick={(e) => this.selectRow(e, row.name)}
								className={cx({selectedRow: row.name === this.props.representativeStore.selectedRep})}
							>
								<TableCell component="th" scope="row">
									{row.name}
								</TableCell>
								<TableCell align="right">{row.party.charAt(0)}</TableCell>
							</TableRow>
						))}
						</TableBody>
					</Table>
				</TableContainer>
			</div>
		)
	}
	selectRow(event, name) {
		this.props.representativeStore.setSelectedRep(name);
	}
}

export default inject('representativeStore')(observer(ListReps));
