import React from 'react';
import { observer, inject } from 'mobx-react';
import { List, ListItem, ListItemText } from '@material-ui/core';
import { styles } from './repInfo.css'
import classNames from 'classnames/bind';

const _ = require('lodash');
const cx = classNames.bind(styles);

class RepInfo extends React.Component {
	render() {
		const representativeInfo = _.find(this.props.representativeStore.representatives, {name: this.props.representativeStore.selectedRep});
		
		return (
			<div className={cx('detailsDisplay')}>
				<h2>Info</h2>
				<List>
					<ListItem>
						<ListItemText primary={`First Name: ${_.first(_.split(_.get(representativeInfo, 'name'), ' '))}`}/>
					</ListItem>
					<ListItem>
						<ListItemText primary={`Last Name: ${_.last(_.split(_.get(representativeInfo, 'name'), ' '))}`}/>
					</ListItem>
					<ListItem>
						<ListItemText primary={`District: ${_.get(representativeInfo, 'district')||''}`}/>
					</ListItem>
					<ListItem>
						<ListItemText primary={`Phone: ${_.get(representativeInfo, 'phone')||''}`}/>
					</ListItem>
					<ListItem>
						<ListItemText primary={`Office: ${_.get(representativeInfo, 'office')||''}`}/>
					</ListItem>
				</List>
			</div>
		)
	}
}

export default inject('representativeStore')(observer(RepInfo));
